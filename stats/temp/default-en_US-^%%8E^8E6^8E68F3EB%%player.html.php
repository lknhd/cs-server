<?php /* Smarty version 2.6.18, created on 2018-07-09 12:15:06
         compiled from default/player.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'url', 'default/player.html', 10, false),array('function', 'flagimg', 'default/player.html', 26, false),array('function', 'iconimg', 'default/player.html', 26, false),array('function', 'ofc', 'default/player.html', 45, false),array('function', 'rankchange', 'default/player.html', 53, false),array('function', 'skillchange', 'default/player.html', 62, false),array('function', 'pctbar', 'default/player.html', 72, false),array('function', 'cycle', 'default/player.html', 221, false),array('modifier', 'escape', 'default/player.html', 15, false),array('modifier', 'commify', 'default/player.html', 26, false),array('modifier', 'compacttime', 'default/player.html', 118, false),array('modifier', 'datetime', 'default/player.html', 122, false),array('modifier', 'int2ip', 'default/player.html', 257, false),array('modifier', 'default', 'default/player.html', 291, false),)), $this); ?>
<!--outermost page container for all content-->
<div id="ps-page-container">

<!--inner container for the content-->
<div id="ps-main">

	<div id="ps-page-title">
		<div class="inner">
			<span>[ 
				<a href="<?php echo smarty_function_url(array('_base' => 'plrhist.php','id' => $this->_tpl_vars['plr']['plrid']), $this);?>
">View History</a> 
				<?php if (ps_user_can_edit_player ( $this->_tpl_vars['plr'] )): ?>
				| <a href="<?php echo smarty_function_url(array('_base' => 'editplr.php','_ref' => 1,'id' => $this->_tpl_vars['plr']['plrid']), $this);?>
">Edit Player</a>
				<?php endif; ?>
			]</span>
			<h1>Player Statistics :: <?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</h1>
			<?php if ($this->_tpl_vars['plr']['clanid']): ?>
			<span>Clan member [ 
				<a href="<?php echo smarty_function_url(array('_base' => 'clan.php','id' => $this->_tpl_vars['plr']['clanid']), $this);?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['clan']['clantag'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</a> 
				<?php if (ps_user_can_edit_clan ( $this->_tpl_vars['plr']['clanid'] , $this->_tpl_vars['plr'] )): ?>
				| <a href="<?php echo smarty_function_url(array('_base' => 'editclan.php','_ref' => 1,'id' => $this->_tpl_vars['plr']['clanid']), $this);?>
">Edit Clan</a>
				<?php endif; ?>
			]</span>
			<?php endif; ?>

			<?php if ($this->_tpl_vars['plr']['rank']): ?>
			<h2><?php echo smarty_function_flagimg(array('cc' => $this->_tpl_vars['plr']['cc']), $this);?>
 <?php echo smarty_function_iconimg(array('icon' => $this->_tpl_vars['plr']['icon']), $this);?>
 <?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
 is ranked <strong>#<?php echo $this->_tpl_vars['plr']['rank']; ?>
</strong> out of <strong><?php echo ((is_array($_tmp=$this->_tpl_vars['totalranked'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</strong><?php if ($this->_tpl_vars['top1percentile']): ?> (top 1 percentile)<?php endif; ?></h2>
			<?php else: ?>
			<h2><?php echo smarty_function_flagimg(array('cc' => $this->_tpl_vars['plr']['cc']), $this);?>
 <?php echo smarty_function_iconimg(array('icon' => $this->_tpl_vars['plr']['icon']), $this);?>
 <?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
 is not ranked</h2>
			<?php endif; ?>
		</div>
		<?php if ($this->_tpl_vars['conf']['theme']['permissions']['show_plr_profile']): ?>
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../default/player_profile.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<?php endif; ?>
	</div>

<?php if ($this->_tpl_vars['plr']['logo']): ?><div class="ps-plr-logo"><?php echo $this->_tpl_vars['plr']['logo']; ?>
</div><?php endif; ?>

<!--left column block -->
<div id="ps-main-column">

<div class="ps-column-frame">
	<?php echo smarty_function_ofc(array('data' => "skill&id=".($this->_tpl_vars['id']),'width' => 287,'height' => 180), $this);?>

</div>

<div class="ps-column-frame<?php if ($this->_tpl_vars['shades']['s_plr_rundown']): ?> s-closed<?php endif; ?>">
<div class="ps-column-header"><a href="" onclick="return false"><span>Player Rundown</span></a></div>
<div id="s-plr-rundown" class="ps-column-content">
<p class="hl firstrow">
	<label>Rank</label>
	<em><?php echo smarty_function_rankchange(array('plr' => $this->_tpl_vars['plr']), $this);?>
</em>
	<span><?php if ($this->_tpl_vars['plr']['rank']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['rank'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
<?php else: ?>-<?php endif; ?></span>
</p>
<p class="substat">
	<label>Previous Rank</label>
	<span><?php if ($this->_tpl_vars['plr']['prevrank']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['prevrank'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
<?php else: ?>-<?php endif; ?></span>
</p>
<p class="hl">
	<label>Skill</label>
	<em><?php echo smarty_function_skillchange(array('plr' => $this->_tpl_vars['plr']), $this);?>
</em>
	<span><?php echo $this->_tpl_vars['plr']['skill']; ?>
</span>
</p>
<p class="substat">
	<label>Previous Skill</label>
	<span><?php echo $this->_tpl_vars['plr']['prevskill']; ?>
</span>
</p>
<p>
	<label>Activity Level</label>
	<em><?php if ($this->_tpl_vars['plr']['activity'] != -1): ?><?php echo smarty_function_pctbar(array('pct' => $this->_tpl_vars['plr']['activity'],'width' => 50,'fixedwidth' => true), $this);?>
<?php else: ?>-<?php endif; ?></em>
	<span><?php if ($this->_tpl_vars['plr']['activity'] != -1): ?><?php echo $this->_tpl_vars['plr']['activity']; ?>
%<?php else: ?>-<?php endif; ?></span>
</p>
<p>
	<label>Bonus Points</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['totalbonus'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<p>
	<label>Total Games</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['games'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<p>
	<label>Total Rounds</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['rounds'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<p>
	<label>Maps Played</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['totalmaps'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<p>
<?php if ($this->_tpl_vars['plr']['totalawards']): ?>
	<label><a href="<?php echo smarty_function_url(array('_base' => 'awards.php','p' => $this->_tpl_vars['plr']['plrid']), $this);?>
">Total Awards</a></label>
	<span><a href="<?php echo smarty_function_url(array('_base' => 'awards.php','p' => $this->_tpl_vars['plr']['plrid']), $this);?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['totalawards'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</a></span>
<?php else: ?>
	<label>Total Awards</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['totalawards'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
<?php endif; ?>
</p>
<p>
	<label>Total Kicks</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['kicked'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<p>
	<label>Total Bans</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['banned'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<p>
	<label>Total Connections</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['connections'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<p>
	<label>Online Time</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['onlinetime'])) ? $this->_run_mod_handler('compacttime', true, $_tmp) : smarty_modifier_compacttime($_tmp)); ?>
</span>
</p>
<p>
	<label>Last Seen</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['lasttime'])) ? $this->_run_mod_handler('datetime', true, $_tmp) : smarty_modifier_datetime($_tmp)); ?>
</span>
</p>
<p>
	<label>First Seen</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['firstseen'])) ? $this->_run_mod_handler('datetime', true, $_tmp) : smarty_modifier_datetime($_tmp)); ?>
</span>
</p>
</div>
</div>

<div class="ps-column-frame<?php if ($this->_tpl_vars['shades']['s_plr_killprofile']): ?> s-closed<?php endif; ?>">
<div class="ps-column-header"><a href="" onclick="return false"><span>Kill Profile</span></a></div>
<div id="s-plr-killprofile" class="ps-column-content">
<p class="firstrow hl">
	<label>Total Kills</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['kills'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<p class="substat">
	<label>Headshot Kills</label>
	<?php if ($this->_tpl_vars['plr']['headshotkills']): ?><em><?php echo $this->_tpl_vars['plr']['headshotkillspct']; ?>
%</em><?php endif; ?>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['headshotkills'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<p class="substat">
	<label>Friendly Fire</label>
	<?php if ($this->_tpl_vars['plr']['ffkills']): ?><em><?php echo $this->_tpl_vars['plr']['ffkillspct']; ?>
%</em><?php endif; ?>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['ffkills'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<p class="hl">
	<label>Total Deaths</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['deaths'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<p class="substat">
	<label>Friendly Fire</label>
	<?php if ($this->_tpl_vars['plr']['ffdeaths']): ?><em><?php echo $this->_tpl_vars['plr']['ffdeathspct']; ?>
%</em><?php endif; ?>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['ffdeaths'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<p class="hl">
	<label>Overall Accuracy</label>
	<span><?php echo $this->_tpl_vars['plr']['accuracy']; ?>
%</span>
</p>
<p class="substat">
	<label>Shots Fired</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['shots'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<p class="substat">
	<label>Shots Hit</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['hits'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<p>
	<label>Kills per Death</label>
	<span><?php echo $this->_tpl_vars['plr']['killsperdeath']; ?>
</span>
</p>
<p>
	<label>Kills per Minute</label>
	<span><?php echo $this->_tpl_vars['plr']['killsperminute']; ?>
</span>
</p>
<p>
	<label>Kill Streak</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['kills_streak'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<p>
	<label>Death Streak</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['deaths_streak'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<p>
	<label>Shots per Kill</label>
	<span><?php echo $this->_tpl_vars['plr']['shotsperkill']; ?>
</span>
</p>
<p>
	<label>Damage Done</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['damage'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<p>
	<label>Suicides</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['suicides'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
</div>
</div>

<div id="ps-id-plrsess" class="ps-column-frame<?php if ($this->_tpl_vars['shades']['s_plrsess']): ?> s-closed<?php endif; ?>">
<div class="ps-column-header"><a href="" onclick="return false"><span>Player Session Chart</span></a></div>
<div id="s-plrsess" class="ps-column-content" style="text-align: center">
	<img src="<?php echo smarty_function_url(array('_base' => 'imgsess.php','id' => $this->_tpl_vars['plr']['plrid']), $this);?>
" alt="[img]" />
</div>
</div>

<?php echo $this->_tpl_vars['player_left_column_mod']; ?>
 
<div class="ps-column-sep">
	<p>Only the top <?php echo $this->_tpl_vars['max_plr_ids']; ?>
 player IDs are shown</p>
</div>

<div id="ps-id-plrname" class="ps-column-frame<?php if ($this->_tpl_vars['shades']['s_plrname']): ?> s-closed<?php endif; ?>">
<div class="ps-column-header"><a href="" onclick="return false"><span>Player Names</span></a></div>
<div id="s-plrname" class="ps-column-content">
<p class="hl firstrow">
	<label>Name</label>
	<span><b>Used</b></span>
</p>
<?php $_from = $this->_tpl_vars['plr']['ids_name']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<p<?php echo smarty_function_cycle(array('values' => ", class='even'"), $this);?>
>
	<label class="name"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['id'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['totaluses'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<?php endforeach; endif; unset($_from); ?>
</div>
</div>

<?php if ($this->_tpl_vars['show_worldids']): ?>
<div id="ps-id-plrworldid" class="ps-column-frame<?php if ($this->_tpl_vars['shades']['s_plrworldid']): ?> s-closed<?php endif; ?>">
<div class="ps-column-header"><a href="" onclick="return false"><span>Player <?php echo $this->_tpl_vars['worldid_noun_plural']; ?>
</span></a></div>
<div id="s-plrworldid" class="ps-column-content">
<p class="hl firstrow">
	<label><?php echo $this->_tpl_vars['worldid_noun']; ?>
</label>
	<span><b>Used</b></span>
</p>
<?php $_from = $this->_tpl_vars['plr']['ids_worldid']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<p<?php echo smarty_function_cycle(array('values' => ", class='even'"), $this);?>
>
	<label><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['id'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['totaluses'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<?php endforeach; endif; unset($_from); ?>
</div>
</div>
<?php endif; ?>

<?php if ($this->_tpl_vars['show_ips']): ?>
<div id="ps-id-plripaddr" class="ps-column-frame<?php if ($this->_tpl_vars['shades']['s_plripaddr']): ?> s-closed<?php endif; ?>">
<div class="ps-column-header"><a href="" onclick="return false"><span>Player IP Addresses</span></a></div>
<div id="s-plripaddr" class="ps-column-content">
<p class="hl firstrow">
	<label>IP</label>
	<span><b>Used</b></span>
</p>
<?php $_from = $this->_tpl_vars['plr']['ids_ipaddr']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<p<?php echo smarty_function_cycle(array('values' => ", class='even'"), $this);?>
>
	<label><?php if ($this->_tpl_vars['i']['id']): ?><?php echo $this->_tpl_vars['i']['flagimg']; ?>
 <?php echo ((is_array($_tmp=$this->_tpl_vars['i']['id'])) ? $this->_run_mod_handler('int2ip', true, $_tmp) : smarty_modifier_int2ip($_tmp)); ?>
<?php else: ?>unknown<?php endif; ?></label>
	<span><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['totaluses'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
</span>
</p>
<?php endforeach; endif; unset($_from); ?>
</div>
</div>
<?php endif; ?>

</div>
<!--end of left column -->

<!--content block-->
<div id="ps-main-content" class="ps-page-player">

	<!--#SESSIONS#-->
	<a name="sessions"></a>
	<div class="ps-table-frame<?php if ($this->_tpl_vars['shades']['s_plrsess']): ?> s-closed<?php endif; ?>">
	<div class="ps-frame-header"><a href="" onclick="return false"><span><?php echo $this->_tpl_vars['plr']['totalsessions']; ?>
 Player Game Sessions</span></a></div>
	<div id="s-plrsess" class="ps-table-inner">
	<?php echo $this->_tpl_vars['sessions_table']; ?>

	<?php if ($this->_tpl_vars['sessionpager']): ?><div class="ps-table-footer"><?php echo $this->_tpl_vars['sessionpager']; ?>
</div><?php endif; ?>
	</div>
	</div>
	<!---->

<?php if ($this->_tpl_vars['conf']['theme']['permissions']['show_hitbox']): ?>
	<div class="ps-table-frame<?php if ($this->_tpl_vars['shades']['s_hitbox']): ?> s-closed<?php endif; ?>">
	<div class="ps-frame-header"><a href="" onclick="return false"><span>Player Hitbox</span></a></div>
	<div id="s-hitbox" class="ps-table-inner">

		<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="277" height="330">
		<param name="allowScriptAccess" value="sameDomain" />
		<param name="movie" value="<?php echo $this->_reg_objects['theme'][0]->parent_url(null);?>
/hitbox/hitbox.swf?<?php echo $this->_tpl_vars['hitbox_url']; ?>
" />
		<param name="quality" value="high" />
		<param name="bgcolor" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['hitbox_bgcolor'])) ? $this->_run_mod_handler('default', true, $_tmp, '#C4C4C4') : smarty_modifier_default($_tmp, '#C4C4C4')); ?>
" />
		<embed src="<?php echo $this->_reg_objects['theme'][0]->parent_url(null);?>
/hitbox/hitbox.swf?<?php echo $this->_tpl_vars['hitbox_url']; ?>
" 
			quality="high" 
			bgcolor="<?php echo ((is_array($_tmp=@$this->_tpl_vars['hitbox_bgcolor'])) ? $this->_run_mod_handler('default', true, $_tmp, '#C4C4C4') : smarty_modifier_default($_tmp, '#C4C4C4')); ?>
" 
			width="277" 
			height="330" 
			name="hitbox" 
			align="middle" 
			allowScriptAccess="sameDomain" 
			type="application/x-shockwave-flash" 
			pluginspage="http://www.macromedia.com/go/getflashplayer" />
		</object>

	</div>
	</div>
<?php endif; ?>

	<!--#WEAPONS#-->
	<a name="weapons"></a>
	<div class="ps-table-frame<?php if ($this->_tpl_vars['shades']['s_plrweapons']): ?> s-closed<?php endif; ?>">
	<div class="ps-frame-header"><a href="" onclick="return false"><span><?php echo $this->_tpl_vars['plr']['totalweapons']; ?>
 Player Weapons</span></a></div>
	<div id="s-plrweapons" class="ps-table-inner">
	<?php echo $this->_tpl_vars['weapons_table']; ?>

	<?php if ($this->_tpl_vars['weaponpager']): ?><div class="ps-table-footer"><?php echo $this->_tpl_vars['weaponpager']; ?>
</div><?php endif; ?>
	</div>
	</div>
	<!---->


	<!--#MAPS#-->
	<a name="maps"></a>
	<div class="ps-table-frame<?php if ($this->_tpl_vars['shades']['s_plrmaps']): ?> s-closed<?php endif; ?>">
	<div class="ps-frame-header"><a href="" onclick="return false"><span><?php echo $this->_tpl_vars['plr']['totalmaps']; ?>
 Player Maps</span></a></div>
	<div id="s-plrmaps" class="ps-table-inner">
	<?php echo $this->_tpl_vars['maps_table']; ?>

	<?php if ($this->_tpl_vars['mappager']): ?><div class="ps-table-footer"><?php echo $this->_tpl_vars['mappager']; ?>
</div><?php endif; ?>
	</div>
	</div>
	<!---->


<?php if ($this->_tpl_vars['use_roles']): ?>
	<!--#ROLES#-->
	<a name="roles"></a>
	<div class="ps-table-frame<?php if ($this->_tpl_vars['shades']['s_plrroles']): ?> s-closed<?php endif; ?>">
	<div class="ps-frame-header"><a href="" onclick="return false"><span><?php echo $this->_tpl_vars['plr']['totalroles']; ?>
 Player Roles</span></a></div>
	<div id="s-plrroles" class="ps-table-inner">
	<?php echo $this->_tpl_vars['roles_table']; ?>

	<?php if ($this->_tpl_vars['rolepager']): ?><div class="ps-table-footer"><?php echo $this->_tpl_vars['rolepager']; ?>
</div><?php endif; ?>
	</div>
	</div>
	<!---->
<?php endif; ?>

	<!--#VICTIMS#-->
	<a name="victims"></a>
	<div class="ps-table-frame<?php if ($this->_tpl_vars['shades']['s_plrvictims']): ?> s-closed<?php endif; ?>">
	<div class="ps-frame-header"><a href="" onclick="return false"><span><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['totalvictims'])) ? $this->_run_mod_handler('commify', true, $_tmp) : smarty_modifier_commify($_tmp)); ?>
 Player Victims</span></a></div>
	<div id="s-plrvictims" class="ps-table-inner">
	<?php echo $this->_tpl_vars['victims_table']; ?>

	<?php if ($this->_tpl_vars['victimpager']): ?><div class="ps-table-footer"><?php echo $this->_tpl_vars['victimpager']; ?>
</div><?php endif; ?>
	</div>
	</div>
	<!---->

</div> 
</div> 
</div> 