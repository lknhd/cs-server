<?php /* Smarty version 2.6.18, created on 2018-07-09 12:15:06
         compiled from default/../default/player_profile.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'default/../default/player_profile.html', 12, false),array('modifier', 'urltruncate', 'default/../default/player_profile.html', 17, false),)), $this); ?>
<div class="ps-plr-profile">
	<?php if ($this->_tpl_vars['conf']['theme']['map']['google_key'] && $this->_tpl_vars['plr']['latitude'] && $this->_tpl_vars['plr']['longitude']): ?>
	<script type="text/javascript">
		var plr_lat = parseInt('<?php echo $this->_tpl_vars['plr']['latitude']; ?>
');
		var plr_lng = parseInt('<?php echo $this->_tpl_vars['plr']['longitude']; ?>
');
	</script>
	<div id="map"></div>
	<?php endif; ?>
	<?php if ($this->_tpl_vars['plr']['steam_community_url']): ?>
	<div class="row">
		<label>Steam Profile:</label>
		<p><a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['steam_community_url'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['steam_community_url'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</a> <em class="add-friend">(<a href="<?php echo $this->_tpl_vars['plr']['steam_add_friend_url']; ?>
">Add Friend</a>)</em></p>
	</div>
	<?php endif; ?>
	<div class="row">
		<label>Website:</label>
		<p><?php if ($this->_tpl_vars['plr']['website']): ?><a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['website'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['plr']['website'])) ? $this->_run_mod_handler('urltruncate', true, $_tmp, 60) : smarty_modifier_urltruncate($_tmp, 60)))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</a><?php else: ?><em>Not Set</em><?php endif; ?></p>
	</div>
	<div class="row">
		<label>Email Address:</label>
		<p><?php if ($this->_tpl_vars['plr']['email']): ?><a href="mailto:<?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['email'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['email'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</a><?php else: ?><em>Not Set</em><?php endif; ?></p>
	</div>
	<div class="row">
		<label>AIM Name:</label>
		<p><?php if ($this->_tpl_vars['plr']['aim']): ?><a href="aim:goim?screenname=<?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['aim'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['aim'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</a><?php else: ?><em>Not Set</em><?php endif; ?></p>
	</div>
	<div class="row">
		<label>ICQ Number:</label>
		<p><?php if ($this->_tpl_vars['plr']['icq']): ?><a href="http://wwp.icq.com/scripts/search.dll?to=<?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['icq'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['icq'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</a><?php else: ?><em>Not Set</em><?php endif; ?></p>
	</div>
	<div class="row">
		<label>MSN Messenger:</label>
		<p><?php if ($this->_tpl_vars['plr']['msn']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['plr']['msn'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<?php else: ?><em>Not Set</em><?php endif; ?></p>
	</div>
</div>